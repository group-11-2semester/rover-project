import machine
#Servo4 = pin21 : 105 closed - 130 open
#Servo1 = pin23 : 20 130
#Servo3 = 19 : 35 130
#Servo2 = 22 : 80 130

class SG90:
    def __init__(self, pin, min, max, freq = 50, start_duty = -1):
        self.pin = machine.Pin(pin)
        self.pwm = machine.PWM(self.pin, freq)
        self.min = min
        self.max = max
        self.freq = freq
        if start_duty != -1 and start_duty > self.min and start_duty < self.max:
            self.pwm.duty(start_duty)
        self.prev_pos = start_duty
        
    def move(self, step):
        new_pos = self.prev_pos + int(step)
        if new_pos < self.min or new_pos > self.max:
            return
        self.pwm.duty(new_pos)
        self.prev_pos = new_pos
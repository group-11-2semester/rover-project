# Complete project details at https://RandomNerdTutorials.com

try:
  import usocket as socket
except:
  import socket

from machine import Pin
import network

import esp
esp.osdebug(None)

import gc

from machine import PWM
import time
gc.collect()

buzzer = PWM(Pin(32))
buzzer.freq(1500)

print('Boot')
for i in range(2):
    buzzer.duty(1000)
    time.sleep(0.05)
    buzzer.duty(0)
    time.sleep(0.05)


ssid = 'Nokia 5.3'
password = '1234qwer'

station = network.WLAN(network.STA_IF)

station.active(True)
station.connect(ssid, password)

while station.isconnected() == False:
  pass

print('Connection successful')
print(station.ifconfig())

led = Pin(2, Pin.OUT)

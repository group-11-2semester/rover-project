#range
from gc import collect
from hcsr04 import HCSR04
from time import sleep
import _thread
import globalVar
right_sensor  = HCSR04(trigger_pin = 18, echo_pin = 5, echo_timeout_us = 10000)
left_sensor = HCSR04(trigger_pin = 33, echo_pin = 25, echo_timeout_us = 10000)

right_running = [20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20]
left_running  = [20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20]

right_running_avg = 20.0
left_running_avg = 20.0

#motor
from dcmotor import DCMotor
from machine import Pin, PWM
from time import sleep

frequency = 15000
#right motor
STBY_D1 = Pin(4, Pin.OUT) # Replace x

PHA_D1A = Pin(17, Pin.OUT) # Replace y
enable_D1A = PWM(Pin(16), frequency)

PHA_D1B = Pin(0, Pin.OUT) # Replace y
enable_D1B = PWM(Pin(15), frequency)

dc_motor_right_1 = DCMotor(STBY_D1, PHA_D1A, enable_D1A)
dc_motor_right_2 = DCMotor(STBY_D1, PHA_D1B, enable_D1B)

#left motor
STBY_D2 = Pin(14, Pin.OUT) # Replace x

PHA_D2A = Pin(13, Pin.OUT) # Replace y
enable_D2A = PWM(Pin(12), frequency)

PHA_D2B = Pin(27, Pin.OUT) # Replace y
enable_D2B = PWM(Pin(26), frequency)

dc_motor_left_1 = DCMotor(STBY_D2, PHA_D2A, enable_D2A)
dc_motor_left_2 = DCMotor(STBY_D2, PHA_D2B, enable_D2B)


def collectdata():
    global right_running
    global left_running

    global right_running_avg
    global left_running_avg
    #make data collection a tread so it is allways running
    sensor_right = right_sensor.distance_cm()
    if sensor_right < 50:
        right_running.append(sensor_right)
    else :
        right_running.append(40)
    del right_running[0]
    
    right_running_avg=round(sum(right_running)/len(right_running),1)

    sensor_left = left_sensor.distance_cm()
    if sensor_left < 50:
        left_running.append(sensor_left)
    else :
        left_running.append(40)
    del left_running[0]
    left_running_avg=round(sum(left_running)/len(left_running),1)
    # end of data collection
right_running_avg = 100
left_running_avg = 100
pre_rra = 100
pre_lra = 100
def autopilot():
    global right_running_avg
    global left_running_avg
    global pre_rra
    global pre_lra
    while True:
        #print('auto')
        if globalVar.killThread == 1:
            _thread.exit()
            globalVar.killThread = 0
    
        collectdata()
        
        #testing
        #print('|    ',left_running_avg,'    :||:    ',right_running_avg,'    |','|    ',min(left_running),'    :||:    ',min(right_running),'    |')
        #sleep(0.005)
        #continue
            
            
        #to know when the rover should just move in a straight line.
        def CheckForLess(list1, val): 
          
            # traverse in the list 
            for x in list1: 
          
                # compare with all the  
                # values with value
                if val <= x and x > 1:
                    return True
            return False
        
        speed=50
        buffer = 0

        #if less than 5cm and decreasing turn opposit
            
        # if distace to one side is above ?30? turn to that side
        if left_running_avg > 50 and right_running_avg > 50:
            #print("drive slow")
            slowspeed = 40
            dc_motor_left_1.forward(slowspeed)
            dc_motor_left_2.forward(slowspeed)
            dc_motor_right_1.forward(slowspeed)
            dc_motor_right_2.forward(slowspeed)
        elif left_running_avg+buffer < right_running_avg and pre_lra-0.1 > left_running_avg or left_running_avg < 10 and pre_lra > left_running_avg:
            print('turn right')
            dc_motor_right_1.backwards(10)
            dc_motor_right_2.backwards(10)
            
            dc_motor_left_1.forward(speed)
            dc_motor_left_2.forward(speed)
        
        elif right_running_avg+buffer < left_running_avg and pre_rra-0.1 > right_running_avg or right_running_avg < 10 and pre_rra > right_running_avg:
            print('turn left')
            
            dc_motor_left_1.backwards(10)
            dc_motor_left_2.backwards(10)
            
            dc_motor_right_1.forward(speed)
            dc_motor_right_2.forward(speed)
            
        else: #drive straight
            #print("drive")
            dc_motor_left_1.forward(speed)
            dc_motor_left_2.forward(speed)
            dc_motor_right_1.forward(speed)
            dc_motor_right_2.forward(speed)
            
        pre_rra = right_running_avg
        pre_lra = left_running_avg
        
        sleep(0.001)


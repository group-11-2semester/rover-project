# Complete project details at https://RandomNerdTutorials.com
import time
from machine import Pin, PWM
import _thread
import globalVar
# motor -------------
from dcmotor import DCMotor
from servo import SG90

beepState = False

def deinit_servos():
    for _key, value in servos.items():
        value.pwm.deinit()

def init_servos():
    for _key, value in servos.items():
        value.pwm = PWM(value.pin, value.freq)

servos = {
    'servo1':SG90(23,20,130,start_duty = 60),
    'servo2':SG90(19,35,130,start_duty = 80),
    'servo3':SG90(22,80,130,start_duty = 105),
    'servo4':SG90(21,105,130,start_duty = 110)
}
time.sleep(1)
deinit_servos()

import autopilot

def deinit_motors(pwm_list):
    for pwm in pwm_list:
        pwm.deinit()

def init_motors():
    frequency = 15000
    #right motor
    STBY_D1 = Pin(4, Pin.OUT) # Replace x

    PHA_D1A = Pin(17, Pin.OUT) # Replace y
    enable_D1A = PWM(Pin(16), frequency)

    PHA_D1B = Pin(0, Pin.OUT) # Replace y
    enable_D1B = PWM(Pin(15), frequency)

    #left motor
    STBY_D2 = Pin(14, Pin.OUT) # Replace x

    PHA_D2A = Pin(13, Pin.OUT) # Replace y
    enable_D2A = PWM(Pin(12), frequency)

    PHA_D2B = Pin(27, Pin.OUT) # Replace y
    enable_D2B = PWM(Pin(26), frequency)

    return [DCMotor(STBY_D1, PHA_D1A, enable_D1A), DCMotor(STBY_D1, PHA_D1B, enable_D1B), DCMotor(STBY_D2, PHA_D2A, enable_D2A), DCMotor(STBY_D2, PHA_D2B, enable_D2B)]
dc_motor_right_1, dc_motor_right_2, dc_motor_left_1, dc_motor_left_2 = init_motors()

buzzer = PWM(Pin(32))
buzzer.freq(1500)

for i in range(3):
    buzzer.duty(500)
    time.sleep(0.05)
    buzzer.duty(0)
    time.sleep(0.05)

def beep():
  buzzer = PWM(Pin(32))
  buzzer.freq(1500)
  while beepState:
      buzzer.duty(1000)
      time.sleep(1)
      buzzer.duty(0)
      time.sleep(1)
  buzzer.duty(0)
  _thread.exit()

motor_state = "Stopped"
mode = 'Driving'
def web_page():
  html = """
  <html>
  <head> 
  <title>Rover Remote Control</title> 
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,"> 
  <style>html{font-family: Helvetica; display:inline-block; margin: 0px auto; text-align: center;}
  
  h1{color: #0F3376; padding: 2vh;}p{font-size: 1.5rem;}.button{user-select: none; display: inline-block; background-color: #e7bd3b; border: none; 
  border-radius: 4px; color: white; padding: 16px 40px; text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}
  .button2{background-color: #4286f4;}</style></head><body> 

  <h2>Controls</h2>
  <p>Rover : <strong>""" + motor_state + """</strong></p>
  <p>
  <a href="/?skidleft"><button class="button">\</button></a>
  <a href="/?forward"><button class="button">^</button></a>
  <a href="/?skidright"><button class="button">/</button></a>
  </p>
  <p>
  <a href="/?left"><button class="button"><</button></a>
  <a href="/?stop"><button class="button">STOP</button></a>
  <a href="/?right"><button class="button">></button></a>
  </p>
  <p><a href="/?backwards"><button class="button">v</button></a></p>
  <p>
  <a href="/?mode"><button class="button">""" + mode + """</button></a>
  <a href="/?auto"><button class="button">AUTO</button></a>
  </p>
  <p>
  <button class="button" data-name="_servo2_1_" onmousedown="start(this)" ontouchstart="start(this)" ontouchend="end()" onmouseup="end()">^</button>
  <button class="button" data-name="_servo2_-1_" onmousedown="start(this)" ontouchstart="start(this)" ontouchend="end()" onmouseup="end()">v</button>
  </p>
  <p>
  <button class="button" data-name="_servo1_1_" onmousedown="start(this)" ontouchstart="start(this)" ontouchend="end()" onmouseup="end()"><</button>
  <button class="button" data-name="_servo4_-1_" onmousedown="start(this)" ontouchstart="start(this)" ontouchend="end()" onmouseup="end()">Close</button>
  <button class="button" data-name="_servo4_1_" onmousedown="start(this)" ontouchstart="start(this)" ontouchend="end()" onmouseup="end()">Open</button>
  <button class="button" data-name="_servo1_-1_" onmousedown="start(this)" ontouchstart="start(this)" ontouchend="end()" onmouseup="end()">></button>
  </p>
  <p>
  <button class="button" data-name="_servo3_1_" onmousedown="start(this)" ontouchstart="start(this)" ontouchend="end()" onmouseup="end()">^</button>
  <button class="button" data-name="_servo3_-1_" onmousedown="start(this)" ontouchstart="start(this)" ontouchend="end()" onmouseup="end()">v</button>
  </p>
  <script>
  var counter;
  function start(btn) {
    var servo_params = btn.getAttribute('data-name');
    counter = setInterval(function(name) {
      var xhr = new XMLHttpRequest();
      xhr.open("GET", "/?moveServo"+servo_params, true);
      xhr.send();
    }, 50);
  }
  function end() {
    clearInterval(counter)
  }
  </script>
  </body>
  </html>"""
  return html

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

speed = 55

while True:
  conn, addr = s.accept()
  #print('Got a connection from %s' % str(addr))
  request = conn.recv(1024)
  request = str(request)
  #print('Content = %s' % request)
  if request.find('/?auto')==6:
      if mode == 'Crane':
          mode = 'Driving'
          deinit_servos()
      globalVar.killThread = 0
      _thread.start_new_thread(autopilot.autopilot, ())
  if request.find('/?mode')==6:
    if mode == 'Driving':
        mode = 'Crane'
        deinit_motors([dc_motor_right_1.enable_pin, dc_motor_right_2.enable_pin, dc_motor_left_1.enable_pin, dc_motor_left_2.enable_pin])
        init_servos()
    else:
        mode = 'Driving'
        deinit_servos()
        dc_motor_right_1, dc_motor_right_2, dc_motor_left_1, dc_motor_left_2 = init_motors()
  
  if request.find('/?forward')==6:
    motor_state="Going Forward"
    dc_motor_left_1.forward(speed)
    dc_motor_left_2.forward(speed)
    dc_motor_right_1.forward(speed)
    dc_motor_right_2.forward(speed)
    
  if request.find('/?skidleft')==6:
    motor_state="Skid Left"
    dc_motor_left_1.backwards(speed)
    dc_motor_left_2.backwards(speed)
    dc_motor_right_1.forward(speed)
    dc_motor_right_2.forward(speed)
    
  if request.find('/?skidright')==6:
    motor_state="Skid Right"
    dc_motor_right_1.backwards(speed)
    dc_motor_right_2.backwards(speed)
    dc_motor_left_1.forward(speed)
    dc_motor_left_2.forward(speed)

  if request.find('/?left')==6:
    motor_state="Going Left"
    dc_motor_right_1.forward(40)
    dc_motor_right_2.forward(40) 
    dc_motor_left_1.stop()
    dc_motor_left_2.stop()  

  if request.find('/?right')==6:
    motor_state="Going Right"
    dc_motor_right_1.stop()
    dc_motor_right_2.stop()
    dc_motor_left_1.forward(40)
    dc_motor_left_2.forward(40)
    
  if request.find('/?stop')==6:
    motor_state="Stopped"
    globalVar.killThread = 1
    dc_motor_right_1.stop()
    dc_motor_right_2.stop()
    dc_motor_left_1.stop()
    dc_motor_left_2.stop()
    
  if request.find('/?backwards')==6:
    motor_state="Backwards"
    dc_motor_left_1.backwards(40)
    dc_motor_left_2.backwards(40)
    dc_motor_right_1.backwards(40)
    dc_motor_right_2.backwards(40)  
  if motor_state =="Backwards":
    beepState = True
    _thread.start_new_thread(beep, ())
  else:
    beepState = False

  if request.find('/?moveServo')==6:
    params = request.split('_')
    servo = params[1]
    direction = params[2]
    servos[servo].move(direction)

  response = web_page()
  conn.send('HTTP/1.1 200 OK\n')
  conn.send('Content-Type: text/html\n')
  conn.send('Connection: close\n\n')
  conn.sendall(response)
  conn.close()